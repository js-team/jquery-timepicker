from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
import os

options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--headless')
options.add_argument('--disable-gpu')
options.add_argument('--disable-dev-shm-usage')
service = ChromeService(executable_path="/usr/bin/chromedriver")
driver = webdriver.Chrome(options=options, service=service)

print("Running tests")
driver.get("http://localhost:8888/SpecRunner.html?random=false")
assert "jquery-ui-timepicker-addon Spec Runner" in driver.title

jasmine_version = WebDriverWait(driver, 60).until(lambda x: x.find_element(By.CLASS_NAME, "jasmine-version"))

print("Ran tests on Jasmine " + jasmine_version.text)

elem = driver.find_element(By.CLASS_NAME, "jasmine-symbol-summary")

for li_node in elem.find_elements(By.TAG_NAME, 'li'):
    li_class = li_node.get_attribute("class")
    if li_class == "jasmine-passed":
        print(".", end = "")
    elif li_class == "jasmine-failed":
        print("x", end = "")
    elif li_class == "jasmine-incomplete":
        print("!", end = "")
    elif li_class == "jasmine-pending":
        print("?", end = "")
    else:
        print("missing class: " + li_class)

print("")
test_result = driver.find_element(By.CLASS_NAME, "jasmine-overall-result")
print(test_result.text)

assert "76 specs" in test_result.text
assert "0 failures" in test_result.text
assert "2 pending specs" in test_result.text
assert "76 specs, 0 failures, 2 pending specs" in test_result.text

driver.close()

print("End of tests")
